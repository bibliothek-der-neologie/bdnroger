# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

FROM docker.io/node:20.14 AS build

WORKDIR /app

COPY package*.json ./

RUN npm ci --omit=dev --ignore-scripts

COPY src src
COPY public public

RUN npm run build


FROM docker.io/nginx:1.27.0-alpine

COPY --from=build /app/build /etc/nginx/html
