<!--
SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

# bdnROGER

App wrapper for the ROGER core component to be used with the BdN.

## Development

Build.

```sh
docker compose build
```

Run.

```sh
docker compose up
```
